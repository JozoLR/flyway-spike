# Spike Flyway

This repository is focusing on how to use flyway within a Java application
It is composed with :
- A docker image containing a PostgreSQL DB
- A simple Java Web application using Flask and Flyway

### Getting ready
- Install Java 11

### Start 
1. Launch the PostgreSQL ```./gradlew startInfra```

2. Start the Java app ```./gradlew startWorker```