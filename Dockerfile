FROM openjdk:11-jre

# Install kafka command line tool (kafkactl) for debugging and investigation purposes
RUN mkdir /etc/kafkactl/
COPY /infra/kafkactl_config.yml /etc/kafkactl/config.yml
COPY /infra/kafkactl*.deb /kafkactl.deb
RUN dpkg -i /kafkactl.deb

# Install the app
COPY /build/distributions/weld-warrior-signals-*.tar /app.tar
RUN mkdir /app/
RUN tar -xvf app.tar --directory app/ --strip-components=1
WORKDIR /app/

COPY ./docker-entrypoint.sh /app/

ENTRYPOINT ["/app/docker-entrypoint.sh"]
CMD ["bin/flyway"]
