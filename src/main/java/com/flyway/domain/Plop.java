package com.flyway.domain;

public class Plop {

    public final String id;
    public final String value;

    public Plop(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public static Plop of(String id, String value) {
        return new Plop(id, value);
    }

}
