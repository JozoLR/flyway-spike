package com.flyway.usecase;

import com.flyway.domain.Plop;
import com.flyway.infrastructure.repository.PlopPostgreRepo;

import java.util.stream.Stream;

public class GetPlop {
    private final PlopPostgreRepo plopRepo;


    public GetPlop(PlopPostgreRepo plopRepo) {
        this.plopRepo = plopRepo;
    }

    public Stream<Plop> handle() {
        return plopRepo.get();
    }
}
