#!/bin/bash
set -e
if [ "$1" = 'bin/flyway' ]; then
    if [ -f SUCCESS ]; then
        rm SUCCESS
    fi
fi
exec "$@"
