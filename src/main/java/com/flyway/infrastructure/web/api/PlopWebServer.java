package com.flyway.infrastructure.web.api;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.flyway.infrastructure.repository.PlopPostgreRepo;
import com.flyway.usecase.GetPlop;
import spark.Request;
import spark.Response;
import spark.Spark;

import java.util.List;
import java.util.stream.Collectors;

import static org.eclipse.jetty.http.MimeTypes.Type.APPLICATION_JSON_UTF_8;
import static spark.Spark.get;
import static spark.Spark.port;

public class PlopWebServer {

    private int port;
    private PlopPostgreRepo plopRepository;

    private PlopWebServer(int port, PlopPostgreRepo plopRepository) {
        this.port = port;
        this.plopRepository = plopRepository;
    }

    public static PlopWebServer create(int port, PlopPostgreRepo plopRepository) {
        return new PlopWebServer(port, plopRepository);
    }

    public void start() {
        port(this.port);

        get("health", ((request, response) -> "UP and RUNNING"));
        get("plop", (this::getPlop));
    }

    private String getPlop(Request request, Response response) {
        response.type(APPLICATION_JSON_UTF_8.toString());
        GetPlop getPlop = new GetPlop(plopRepository);
        var plops = getPlop.handle();
        List<JsonObject> plopsList = plops
                .map(plop -> Json.object().add("value", plop.value))
                .collect(Collectors.toList());

        JsonArray jsonArray = new JsonArray();
        plopsList.forEach(jsonArray::add);

        response.body(jsonArray.asString());
        return response.body();
    }

    public void stop() {
        Spark.stop();
    }

}
