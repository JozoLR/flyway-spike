package com.flyway.infrastructure.repository;

import com.flyway.domain.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.stream.Stream;

public class PlopPostgreRepo {


    private final Connection connection;

    public PlopPostgreRepo(Connection connection) {
        this.connection = connection;
    }

    public void save(Plop plop) {
        Statement statement;
        try {
            statement = connection.createStatement();
            String sql = "INSERT INTO ";
            sql += "plop";
            sql += " (id, value) VALUES (";
            sql += "'" + plop.id + "',";
            sql += "'" + plop.value + "',";

            statement.executeUpdate(sql);
            statement.close();
        } catch (SQLException e) {
            throw new RuntimeException("Something went wrong while saving the signal", e);
        }
    }

    public Stream<Plop> get() {
        ResultSet queryResults;
        ArrayList<Plop> plops;
        try {
            queryResults = queryPlops();
            plops = extractPlop(queryResults);
        } catch (SQLException e) {
            throw new RuntimeException("Something went wrong while retrieving the plops");
        }
        return plops.stream();
    }

    private ArrayList<Plop> extractPlop(ResultSet queryResults) throws SQLException {
        ArrayList<Plop> plops = new ArrayList<>();

        while (queryResults.next()) {
            Plop plop = Plop.of(
                    queryResults.getString("id"),
                    queryResults.getString("value")
            );
            plops.add(plop);
        }
        return plops;
    }

    private ResultSet queryPlops() throws SQLException {
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM plop;";
        return statement.executeQuery(sql);
    }
}
