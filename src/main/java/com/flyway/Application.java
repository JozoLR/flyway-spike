package com.flyway;

import com.flyway.infrastructure.repository.PlopPostgreRepo;
import com.flyway.infrastructure.web.api.PlopWebServer;
import org.flywaydb.core.Flyway;

import java.sql.DriverManager;
import java.sql.SQLException;

public class Application {
    public static void main(String[] args) throws RuntimeException, SQLException {
        String database_url = System.getenv("POSTGRESQL_URL");
        String user = System.getenv("USER");
        String password = System.getenv("PASSWORD");

        updateDatabaseSchema(database_url, user, password);

        var connection = DriverManager.getConnection(database_url, user, password);

        PlopPostgreRepo plopRepository = new PlopPostgreRepo(connection);
        PlopWebServer.create(3000, plopRepository).start();
    }

    private static void updateDatabaseSchema(String database_url, String user, String password) {
        Flyway flyway = Flyway.configure()
                .dataSource(database_url, user, password)
                .load();

        flyway.baseline();
        flyway.migrate();
    }
}
